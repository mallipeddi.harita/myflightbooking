$(document).ready(function () {

    $("#search-form").submit(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();

        //client side validation for passangers
        if( ($("#noOfPassangers").val() == '') |  ($("#noOfPassangers").val() == 0)) {
            alert("Enter a valid number of passangers");
            return;
        }


        fire_ajax_submit();

    });

});

function fire_ajax_submit() {

    var search
        = {}
    search["source"] = $("#source").val();
    search["destination"] = $("#destination").val();
    search["noOfPassangers"] = $("#noOfPassangers").val();
    search["startDate"] = $("#startDate").val();
    search["classType"] = $('#classType option:selected').val();


    $("#btn-search").prop("disabled", true);

    console.log(JSON.stringify(search));
    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/searchFlightsViaAjax",
        data: JSON.stringify(search),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {


            var json = "<h4>Flights matching the criteria</h4><pre>"
                + JSON.stringify(data, null, 4) + "</pre>";
            $('#searchResults').html(json);

            $("#btn-search").prop("disabled", false);

        },
        error: function (e) {
            var json = "<h4>Error Occurred. Please contact Administrator.</h4><pre>"
                + e.responseText + "</pre>";
            $('#searchResults').html(json);

            console.log("ERROR : ", e);
            $("#btn-search").prop("disabled", false);

        }
    });

}
