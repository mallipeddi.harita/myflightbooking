package org.haritha.flightbooking.com.Service;

import org.haritha.flightbooking.com.Models.Flight;
import org.haritha.flightbooking.com.Models.SearchCriteria;
import org.haritha.flightbooking.com.Repository.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
public class FlightService {

    private FlightRepository repository;

@Autowired
    public  FlightService(FlightRepository repository)
{
 this.repository =  repository;
}


    //   public List<Flight> search(SearchCriteria searchCriteria) {
    public List<Flight> search(SearchCriteria searchCriteria) {
      //  this.repository.loadFlights();

        List<Flight> flights = this.repository.getRepositoryStream();
        return calculateTicketPrice (searchCriteria, flights.stream().filter(i -> ( (i.getSource().toLowerCase().equals(searchCriteria.getSource().toLowerCase().toString()) )
                & ( i.getDestination().toLowerCase().equals(searchCriteria.getDestination().toLowerCase().toString())
        ) ) ) .filter( j -> (j.getTotalSeats() >= (searchCriteria.getNoOfPassangers()))
                        &  ( j.getScheduledDate().equals(searchCriteria.getStartDate())
                        & (j.getSeatsbyClassType(searchCriteria.getClassType().toLowerCase()) >0)
                )
        )
                .collect(Collectors.toList()));

        //calculateTicketPrice(searchCriteria,tempFlights);
        //  return tempFlights;

    }


    public List<Flight>  calculateTicketPrice(SearchCriteria searchCriteria,List<Flight> flights) {
        double tempPrice = 0;
        String classTypeName = searchCriteria.getClassType();
        int availableSeats = 0, totalSeats = 0;
        int numOfTravellers = searchCriteria.getNoOfPassangers();
        LocalDate flyDate = searchCriteria.getStartDate();
        for (Flight flight : flights) {

            tempPrice = flight.getPricebyClassType(classTypeName);

            totalSeats = flight.getSeatsbyClassType(classTypeName);
            availableSeats = flight.getAvailableClassSeats().getSeatsbyClassType(classTypeName);
            switch (searchCriteria.getClassType()) {

            case "ECONOMY":


                    if (numOfTravellers <= availableSeats) {
                        //only 10 percent seats avaialble
                        if (availableSeats <= getSeatsbyPercent(10, totalSeats))
                            //apply 60% on base fare.
                            flight.setTicketPrice((tempPrice + (0.6 * tempPrice)) * numOfTravellers);

                            //first 40 percent seats
                        else if (availableSeats <= getSeatsbyPercent(40, totalSeats))
                            //apply base fare.
                            flight.setTicketPrice(tempPrice * numOfTravellers);
                        else
                            flight.setTicketPrice((tempPrice + (0.3 * tempPrice)) * numOfTravellers);

                    }
                    else
                    {
                        flight.setMessage("Please select  number of passangers with in availableSeats " + availableSeats);
                    }
                    //Need to respond is no of passangers greater hean available seats

                break;
            case "BUSINESSCLASS":
                    if (numOfTravellers <= availableSeats) {

                        if ((flyDate.getDayOfWeek() == DayOfWeek.FRIDAY) |
                                (flyDate.getDayOfWeek() == DayOfWeek.MONDAY) |
                                (flyDate.getDayOfWeek() == DayOfWeek.SUNDAY)) {
                            flight.setTicketPrice((tempPrice + (0.4 * tempPrice)) * numOfTravellers);
                        } else
                            flight.setTicketPrice(tempPrice * numOfTravellers);
                    }
                    else
                    {
                        flight.setMessage("Please select  number of passangers with in availableSeats " + availableSeats);
                    }
//                }
                break;
            case "FIRSTCLASS":
                    //Open booking only when 10 days left
                    if ((LocalDate.now().isAfter(flight.getScheduledDate().minusDays(10)))) {
                        {
                            if (numOfTravellers <= availableSeats) {


                                long daysBetween = DAYS.between(LocalDate.now(), flight.getScheduledDate());
                                if(daysBetween==0)
                                    flight.setTicketPrice((tempPrice + (10 * 0.1 * tempPrice)) * numOfTravellers);
                                else
                                  flight.setTicketPrice((tempPrice + ((10-daysBetween) * 0.1 * tempPrice)) * numOfTravellers);

                            }
                            else
                            {
                                flight.setMessage("Please select  number of passangers with in availableSeats " + availableSeats);
                            }
                        }



                    }
                    else
                    {
                        flight.setMessage("First Class booking can be opened only before 10 days of travel.");
                    }
                    break;
//                }
        }
        }
        return flights;

    }

    public  int getSeatsbyPercent(int percent,int seatsbyClass)
    {

        return seatsbyClass * ( percent/100);
    }

}
