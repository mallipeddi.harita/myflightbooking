package org.haritha.flightbooking.com.Models;

import java.time.LocalDate;

public class Route {

    private int routeId;
    private int flightNo;
    private String source;
    private String destination;
    private int totalAvailableSeats;
    private LocalDate scheduledDate;
    private  ClassSeat classSeat;

    public void setClassSeats(ClassSeat classSeat) {
        this.classSeat = classSeat;
    }

    public void setTotalAvailableSeats(int totalAvailableSeats) {
        this.totalAvailableSeats = totalAvailableSeats;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public void setFlightNo(int flightNo) {
        this.flightNo = flightNo;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setScheduledDate(LocalDate scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    public ClassSeat getClassSeat() {
        return this.classSeat;
    }

    public int getFlightNo() {
        return flightNo;
    }

    public int getRouteId() {
        return routeId;
    }

    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }

    public LocalDate getScheduledDate() {
        return scheduledDate;
    }

    public int getTotalAvailableSeats() {

        totalAvailableSeats= this.getClassSeat().getTotalAvailableSeats("");
        return totalAvailableSeats;
    }

    public Route(int routeId, int flightNo, String source, String destination,ClassSeat classSeat, LocalDate scheduledDate)
    {
        this.setRouteId(routeId);
        this.setFlightNo(flightNo);
        this.setSource(source);
        this.setDestination(destination);
        this.setClassSeats(classSeat);
        this.setTotalAvailableSeats(totalAvailableSeats);
        this.setScheduledDate(scheduledDate);

    }
}
