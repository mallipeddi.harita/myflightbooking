package org.haritha.flightbooking.com.Models;
import java.util.Map;
import java.time.LocalDate;
import java.util.Set;

public class Flight {
    private int flightNo;
    private Route flightRoute;
    private Map<ClassType, Float> priceMatrix;
    private double ticketPrice=0;
    private ClassSeat availableClassSeats;
    private String message;

    public double getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(double ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getFlightNo() {
        return flightNo;
    }

    public void setFlightRoute(Route flightRoute) {
        this.flightRoute = flightRoute;
    }

    public Route getFlightRoute() {
        return flightRoute;
    }

    public float getPricebyClassType(String classtype)
    {
        float basePrice=0;

        // Returns Set view
        Set< Map.Entry<ClassType,Float> > st = this.getPriceMatrix().entrySet();

        for (Map.Entry< ClassType,Float> me:st)
        {
            if(me.getKey().toString().toLowerCase().equals(classtype.toLowerCase()))
                basePrice =  me.getValue();
        }

        return basePrice;
    }

    public void setPriceMatrix(Map<ClassType, Float> priceMatrix) {

        this.priceMatrix = priceMatrix;
    }

    public void setBasePriceperClass(String classTypeName, Float basePrice) {


        if(ClassType.BUSINESSCLASS.toString().toLowerCase().equals(classTypeName.toLowerCase()))
            this.getPriceMatrix().put(ClassType.BUSINESSCLASS,basePrice);
        else if(ClassType.FIRSTCLASS.toString().toLowerCase().equals(classTypeName.toLowerCase()))
            this.getPriceMatrix().put(ClassType.FIRSTCLASS,basePrice);
        else if(ClassType.ECONOMY.toString().toLowerCase().equals(classTypeName.toLowerCase()))
            this.getPriceMatrix().put(ClassType.ECONOMY,basePrice);
    }

    public Map<ClassType, Float> getPriceMatrix() {
        return priceMatrix;
    }

    public String getSource() {
        return this.flightRoute.getSource();
    }

    public String getDestination() {
        return this.flightRoute.getDestination();
    }

    public LocalDate getScheduledDate() {
        return this.flightRoute.getScheduledDate();
    }

    public int getTotalSeats()
    {
        return this.flightRoute.getTotalAvailableSeats();
        //return ( this.getBcSeats() + this.getFcSeats() + this.getEconomySeats());
    }

     public int getSeatsbyClassType(String className){

         return this.flightRoute.getClassSeat().getTotalAvailableSeats(className);
     }

    public void setFlightNo(int flightNo) {
        this.flightNo = flightNo;
    }

    public ClassSeat getAvailableClassSeats() {
        return availableClassSeats;
    }

    public void setAvailableClassSeats(ClassSeat availableClassSeats) {
        this.availableClassSeats = availableClassSeats;
    }
    //default const
   public Flight(int flightNo,Route route, Map<ClassType, Float> priceMatrix,ClassSeat availableclassSeat)

    {
        this.setFlightNo(flightNo);
          this.setFlightRoute(route);
          this.setPriceMatrix(priceMatrix);
          this.setAvailableClassSeats(availableclassSeat);
    }


}
