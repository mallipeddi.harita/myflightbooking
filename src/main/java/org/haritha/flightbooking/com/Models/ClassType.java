package org.haritha.flightbooking.com.Models;


public enum ClassType {
    FIRSTCLASS,
    BUSINESSCLASS,
    ECONOMY
}
