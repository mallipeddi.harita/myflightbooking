package org.haritha.flightbooking.com.Models;
import java.util.List;

    public class AjaxResponseBody {

        String msg;
        List<Flight> result;



        public void setMsg(String msg) {
            this.msg = msg;
        }

        public void setResult(List<Flight> result) {
            this.result = result;
        }

        public List<Flight> getResult() {
            return result;
        }

        public String getMsg() {
            return msg;
        }
    }
