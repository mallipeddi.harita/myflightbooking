
package org.haritha.flightbooking.com.Models;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.util.Set;


public class ClassSeat {



      private  Map< ClassType,Integer> seatsperClass;// = new HashMap<ClassType,Integer>();

    public Map<ClassType, Integer> getSeatsperClass() {
        return seatsperClass;
    }

    public void setSeatsperClass(String classTypeName, int noOfSeats) {


        if(ClassType.BUSINESSCLASS.toString().toLowerCase().equals(classTypeName.toLowerCase()))
              this.seatsperClass.put(ClassType.BUSINESSCLASS,noOfSeats);
        else if(ClassType.FIRSTCLASS.toString().toLowerCase().equals(classTypeName.toLowerCase()))
            this.seatsperClass.put(ClassType.FIRSTCLASS,noOfSeats);
        else if(ClassType.ECONOMY.toString().toLowerCase().equals(classTypeName.toLowerCase()))
            this.seatsperClass.put(ClassType.ECONOMY,noOfSeats);
    }

    public int getTotalAvailableSeats(String classTypeName)
    {
        if(classTypeName=="")
            return this.seatsperClass.values().stream().mapToInt(Integer::intValue).sum();

        else {
            int seats=0;

            Set<Map.Entry<ClassType, Integer>> st = this.seatsperClass.entrySet();

            for (Map.Entry<ClassType, Integer> me : st) {
                if (me.getKey().toString().toLowerCase().equals(classTypeName.toLowerCase()))
                    seats = me.getValue();
            }
            return seats;
        }

    }

//    public Map<ClassType, Integer> getSeatsperClassObject() {
//        return seatsperClass;
//    }

    @Autowired
    public ClassSeat(Map<ClassType,Integer> seatsperClass)
    {
        this.seatsperClass = seatsperClass;

    }

    public String getClassType(ClassType classType)
    {

        // Returns Set view
        Set< Map.Entry<ClassType,Integer> > st = seatsperClass.entrySet();

        for (Map.Entry< ClassType,Integer> me:st)
        {
            if(me.getKey().toString().toLowerCase().equals(classType.name().toLowerCase()))
                return me.getKey().name().toString();
        }

        return null;
    }

    public int getSeatsbyClassType( String classTypeName)
    {
        int seats=0;

        // Returns Set view
        Set< Map.Entry<ClassType,Integer> > st = this.seatsperClass.entrySet();

        for (Map.Entry< ClassType,Integer> me:st)
        {
            if(me.getKey().toString().toLowerCase().equals(classTypeName.toLowerCase()))
                seats =  me.getValue();
        }

        return seats;
    }


}


