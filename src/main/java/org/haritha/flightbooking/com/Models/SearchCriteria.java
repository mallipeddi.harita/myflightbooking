package org.haritha.flightbooking.com.Models;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

public class SearchCriteria {

    @NotBlank(message = "source can't empty!")
    private String source;
    @NotBlank(message = "destination can't empty!")
    private String destination;
    private int noOfPassangers;
    private LocalDate startDate;
    private ClassType classType;

    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }

    public int  getNoOfPassangers() {
        return noOfPassangers;
    }

    public  LocalDate getStartDate() {
        return startDate;
    }

    public String getClassType() {
        return classType.name();
    }

    //    public  LocalDate getEndDate() {
//        return endDate;
//    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public void setNoOfPassangers(int noOfPassangers) {


            this.noOfPassangers = noOfPassangers;
    }

    public void setStartDate( LocalDate startDate) {
            if(startDate == null)
                this.startDate = LocalDate.now().plusDays(1);
            else
                this.startDate =startDate;

    }

    public void setClassType(ClassType classType) {

        this.classType = classType;
    }
    //    public void setEndDate( LocalDate endDate) {
//
//            this.endDate =endDate;
//
//    }


    public SearchCriteria(String source, String destination, int  noOfPassangers, LocalDate startDate,
                          ClassType classType)
    {
        this.setSource(source);
        this.setDestination(destination);
        this.setNoOfPassangers(noOfPassangers);
        this.setStartDate(startDate);
        this.setClassType(classType);

    }
//
//    public SearchCriteria(String source, String destination )
//    {
//        this.setSource(source);
//        this.setDestination(destination);
//        this.setNoOfPassangers(1);
//        this.setStartDate(LocalDate.now().plusDays(1));
////        this.setEndDate(LocalDate.now());
//
//    }
//    public SearchCriteria(String source, String destination,int noOfPassangers )
//    {
//        this.setSource(source);
//        this.setDestination(destination);
//        this.setNoOfPassangers(noOfPassangers);
//        this.setStartDate(LocalDate.now().plusDays(1));
////        this.setEndDate(LocalDate.now());
//
//    }
//    public SearchCriteria()
//    {
//        this.setNoOfPassangers(1);
//        this.setStartDate(LocalDate.now().minusDays(1));
////        this.setEndDate(LocalDate.now());
//    }
}
