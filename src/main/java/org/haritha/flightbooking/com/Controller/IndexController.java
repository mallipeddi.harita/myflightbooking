package org.haritha.flightbooking.com.Controller;

        import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
@Controller
public class IndexController {
  //  private final Logger logger = LoggerFactory.getLogger(IndexController.class);
    @GetMapping("/SearchFlights")
    public String index() {
        return "SearchFlights";
    }
}