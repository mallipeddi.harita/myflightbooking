package org.haritha.flightbooking.com.Controller;

        import org.haritha.flightbooking.com.Models.AjaxResponseBody;
        import org.haritha.flightbooking.com.Models.ClassType;
        import org.haritha.flightbooking.com.Models.Flight;
import org.haritha.flightbooking.com.Models.SearchCriteria;
import org.haritha.flightbooking.com.Repository.FlightRepository;
import org.haritha.flightbooking.com.Service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
        import java.time.LocalDate;
        import java.util.List;
import java.util.stream.Collectors;

@RestController
public class FlightController {

      FlightRepository flightRepository;
    FlightService flightService;

    @Autowired
    public FlightController()
    {
        flightRepository = new FlightRepository();
         flightService = new FlightService(flightRepository);
    }

    @RequestMapping("/Hello")
    public String Greeting()
    {
        return "Hello there on Monday";
    }

   // @RequestMapping("/getFlights/{flightNo}")
    @GetMapping("/searchFlightsByRequestParam")
   public List<Flight> searchFlightsByRequestParam(@RequestParam("source") String source , @RequestParam("destination") String destination,
                                     @RequestParam(name="noOfPassangers" , required = false, defaultValue = "1") int noOfPassangers ) throws Exception {
        SearchCriteria searchCriteria;
        try {
            searchCriteria = new SearchCriteria(source, destination, noOfPassangers, LocalDate.now(), ClassType.ECONOMY);
            return flightService.search(searchCriteria);

        }
        catch (Exception ex) {
                throw ex;
        }
        finally {

        }

    }

    @PostMapping("/searchFlightsViaAjax")
    public ResponseEntity<?> searchFlightsViaAjax(@Valid @RequestBody SearchCriteria searchCriteria, Errors errors) throws Exception {
        AjaxResponseBody result = new AjaxResponseBody();

        //If error, just return a 400 bad request, along with the error message
        if (errors.hasErrors()) {

            result.setMsg(errors.getAllErrors()
                    .stream().map(x -> x.getDefaultMessage())
                    .collect(Collectors.joining(",")));

            return ResponseEntity.badRequest().body(result);

        }

        List<Flight> flights = flightService.search(searchCriteria);
            if (flights.isEmpty()) {
                result.setMsg("No flights found!");
            } else {
                result.setMsg("Book your flight");
            }
            result.setResult(flights);

            return ResponseEntity.ok(result);
    }



}
