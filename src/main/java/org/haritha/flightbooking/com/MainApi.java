package org.haritha.flightbooking.com;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;

@SpringBootApplication
public class MainApi {
    public static void main(String[] args) {
        System.out.println(LocalDate.now());
        SpringApplication.run(MainApi.class,args);
    }
}
