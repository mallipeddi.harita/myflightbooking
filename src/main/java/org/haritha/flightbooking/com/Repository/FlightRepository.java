package org.haritha.flightbooking.com.Repository;

import org.haritha.flightbooking.com.Models.*;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class FlightRepository implements ObjectRepository<Flight> {

    private List<Flight> repository;
    private  SearchCriteria searchCriteria;

    public FlightRepository() {
    }

    @PostConstruct
public void loadFlights()
{
    repository = new ArrayList<Flight>();

    //loading seats per class
    Map<ClassType,Integer> seatsperClass777 = new HashMap<>();
    seatsperClass777.put(ClassType.FIRSTCLASS,8);
    seatsperClass777.put(ClassType.BUSINESSCLASS,35);
    seatsperClass777.put(ClassType.ECONOMY,195);
    ClassSeat classSeats777 = new ClassSeat(seatsperClass777);
    //loading available seats per class
    Map<ClassType,Integer> availableseatsperClass777 = new HashMap<>();
    availableseatsperClass777.put(ClassType.FIRSTCLASS,5);
    availableseatsperClass777.put(ClassType.BUSINESSCLASS,15);
    availableseatsperClass777.put(ClassType.ECONOMY,95);
    ClassSeat availableclassSeats777 = new ClassSeat(availableseatsperClass777);


    Map<ClassType,Float> pricePerClass777 = new HashMap<>();
    pricePerClass777.put(ClassType.FIRSTCLASS, (float) 20000.00);
    pricePerClass777.put(ClassType.BUSINESSCLASS, (float) 13000);
    pricePerClass777.put(ClassType.ECONOMY, (float) 6000);

    //loading seats per class
    Map<ClassType,Integer> seatsperClass319 = new HashMap<>();
    seatsperClass319.put(ClassType.FIRSTCLASS,0);
    seatsperClass319.put(ClassType.BUSINESSCLASS,0);
    seatsperClass319.put(ClassType.ECONOMY,144);
    ClassSeat classSeats319 = new ClassSeat(seatsperClass319);

    //loading available seats per class
    Map<ClassType,Integer> availableseatsperClass319 = new HashMap<>();
    availableseatsperClass319.put(ClassType.FIRSTCLASS,0);
    availableseatsperClass319.put(ClassType.BUSINESSCLASS,0);
    availableseatsperClass319.put(ClassType.ECONOMY,100);
    ClassSeat availableclassSeats319 = new ClassSeat(availableseatsperClass319);

    Map<ClassType,Float> pricePerClass319 = new HashMap<>();
    pricePerClass319.put(ClassType.FIRSTCLASS, (float) 0);
    pricePerClass319.put(ClassType.BUSINESSCLASS, (float) 0);
    pricePerClass319.put(ClassType.ECONOMY, (float) 4000);

    //loading seats per class
    Map<ClassType,Integer> seatsperClass321 = new HashMap<>();
    seatsperClass321.put(ClassType.FIRSTCLASS,0);
    seatsperClass321.put(ClassType.BUSINESSCLASS,20);
    seatsperClass321.put(ClassType.ECONOMY,152);
    ClassSeat classSeats321 = new ClassSeat(seatsperClass321);
    //loading available seats per class
    Map<ClassType,Integer> availableseatsperClass321 = new HashMap<>();
    availableseatsperClass321.put(ClassType.FIRSTCLASS,0);
    availableseatsperClass321.put(ClassType.BUSINESSCLASS,10);
    availableseatsperClass321.put(ClassType.ECONOMY,152);
    ClassSeat availableclassSeats321 = new ClassSeat(availableseatsperClass321);

    Map<ClassType,Float> pricePerClass321 = new HashMap<>();
    pricePerClass321.put(ClassType.FIRSTCLASS, (float) 0);
    pricePerClass321.put(ClassType.BUSINESSCLASS, (float) 10000);
    pricePerClass321.put(ClassType.ECONOMY, (float) 5000);

    repository.add(new Flight(777,new Route(1,777,"HYD","BLR",classSeats777,
            LocalDate.now().plusDays(3)),pricePerClass777,availableclassSeats777));
    repository.add(new Flight(777,new Route(4,777,"BLR","HYD",classSeats777,
            LocalDate.now().plusDays(2)),pricePerClass777,availableclassSeats777));

    repository.add(new Flight(319,new Route(2,319,"HYD","MUM",classSeats319,
            LocalDate.now()),pricePerClass319,availableclassSeats319));
    repository.add(new Flight(319,new Route(5,319,"MUM","HYD",classSeats319,
            LocalDate.now().plusDays(4)),pricePerClass319,availableclassSeats319));

    repository.add(new Flight(321,new Route(3,321,"HYD","DEL",classSeats321,
            LocalDate.now().plusDays(2)),pricePerClass321,availableclassSeats321));
    repository.add(new Flight(321,new Route(6,321,"DEL","HYD",classSeats321,
            LocalDate.now().plusDays(3)),pricePerClass321,availableclassSeats321));
    repository.add(new Flight(333,new Route(7,333,"HYD","BLR",classSeats777,
            LocalDate.now().plusDays(8)),pricePerClass777,availableclassSeats777));
}
//public  SearchCriteria loadSearchCriteria(String source,String destination,int noOfPassangers)
//{
//return new SearchCriteria(source,destination,noOfPassangers);
//}
    @Override
    public void store(Flight flight) {
        loadFlights();
        repository.add(flight);
    }

    @Override
        public Flight retrieve(int flightNo) {
        loadFlights();
        return repository.get(flightNo);
    }

    @Override
    public Flight delete(int flightNo) {
        Flight e = repository.get(flightNo);

        this.repository.remove(flightNo);
        return e;
    }

    public List<Flight> getRepositoryStream()
    {
        loadFlights();
       return repository.stream().collect(Collectors.toList());
    }

}

