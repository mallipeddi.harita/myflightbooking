package org.haritha.flightbooking.com.Repository;

import org.haritha.flightbooking.com.Models.SearchCriteria;

import java.util.List;

public interface ObjectRepository<T> {

    public void store(T t);

    public T retrieve(int id);

    public T delete(int id);
}

