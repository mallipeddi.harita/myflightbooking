package org.haritha.flightbooking.com.Service;

import org.haritha.flightbooking.com.Models.*;
import org.haritha.flightbooking.com.Repository.FlightRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FlightServiceTest {

//    @Mock
//    private FlightRepository flightRepository;

    @Mock
    private FlightService flightService;

@Test
public void returnFlightsgivensearchCriteria()  {

    List<Flight> expectedResult = new ArrayList<>() ;
    //loading seats per class
    Map<ClassType,Integer> seatsperClass777 = new HashMap<>();
    seatsperClass777.put(ClassType.FIRSTCLASS,8);
    seatsperClass777.put(ClassType.BUSINESSCLASS,35);
    seatsperClass777.put(ClassType.ECONOMY,195);
    ClassSeat classSeats777 = new ClassSeat(seatsperClass777);
    //loading available seats per class
    Map<ClassType,Integer> availableseatsperClass777 = new HashMap<>();
    availableseatsperClass777.put(ClassType.FIRSTCLASS,5);
    availableseatsperClass777.put(ClassType.BUSINESSCLASS,15);
    availableseatsperClass777.put(ClassType.ECONOMY,95);
    ClassSeat availableclassSeats777 = new ClassSeat(availableseatsperClass777);
    //Price for each class
    Map<ClassType,Float> pricePerClass777 = new HashMap<>();
    pricePerClass777.put(ClassType.FIRSTCLASS, (float) 20000.00);
    pricePerClass777.put(ClassType.BUSINESSCLASS, (float) 13000);
    pricePerClass777.put(ClassType.ECONOMY, (float) 6000);

    //search results
    expectedResult.add( new Flight(777,new Route(1,777,"HYD","BLR",classSeats777,
            LocalDate.now().plusDays(1)),pricePerClass777,availableclassSeats777));

    SearchCriteria searchCriteria = new SearchCriteria("HYD","BLR",2,LocalDate.now().plusDays(1),
            ClassType.ECONOMY);
    flightService = new FlightService(new FlightRepository());
    List<Flight> filteredFlight = flightService.search(searchCriteria);

    //FlightRepository flightRepository = mock(FlightRepository.class);
     flightService = mock(FlightService.class);
    when(flightService.search(searchCriteria)).thenReturn(expectedResult);


    assertEquals(1,filteredFlight.size());
}

    @Test
    //Test for   null schedule date then return the flights with nextdays flights
    public void returnFlightsgivenSourceAndDestination()  {

        List<Flight> expectedResult = new ArrayList<>() ;
        //loading seats per class
        Map<ClassType,Integer> seatsperClass777 = new HashMap<>();
        seatsperClass777.put(ClassType.FIRSTCLASS,8);
        seatsperClass777.put(ClassType.BUSINESSCLASS,35);
        seatsperClass777.put(ClassType.ECONOMY,195);
        ClassSeat classSeats777 = new ClassSeat(seatsperClass777);
        //loading available seats per class
        Map<ClassType,Integer> availableseatsperClass777 = new HashMap<>();
        availableseatsperClass777.put(ClassType.FIRSTCLASS,5);
        availableseatsperClass777.put(ClassType.BUSINESSCLASS,15);
        availableseatsperClass777.put(ClassType.ECONOMY,95);
        ClassSeat availableclassSeats777 = new ClassSeat(availableseatsperClass777);
        //Price for each class
        Map<ClassType,Float> pricePerClass777 = new HashMap<>();
        pricePerClass777.put(ClassType.FIRSTCLASS, (float) 20000.00);
        pricePerClass777.put(ClassType.BUSINESSCLASS, (float) 13000);
        pricePerClass777.put(ClassType.ECONOMY, (float) 6000);

        //search results
        expectedResult.add( new Flight(777,new Route(1,777,"HYD","BLR",classSeats777,
                LocalDate.now().plusDays(1)),pricePerClass777,availableclassSeats777));

        SearchCriteria searchCriteria = new SearchCriteria("HYD","BLR",1,null,
                ClassType.ECONOMY);
        flightService = new FlightService(new FlightRepository());
        List<Flight> filteredFlight = flightService.search(searchCriteria);

        //FlightRepository flightRepository = mock(FlightRepository.class);
        flightService = mock(FlightService.class);
        when(flightService.search(searchCriteria)).thenReturn(expectedResult);


        assertEquals(1,filteredFlight.size());
    }

    @Test
    //Test for Economy class fare
    public void returnFlightEconomyPricesgivensearchCriteria()  {

        List<Flight> expectedResult = new ArrayList<>() ;
        //loading seats per class
        Map<ClassType,Integer> seatsperClass777 = new HashMap<>();
        seatsperClass777.put(ClassType.FIRSTCLASS,8);
        seatsperClass777.put(ClassType.BUSINESSCLASS,35);
        seatsperClass777.put(ClassType.ECONOMY,195);
        ClassSeat classSeats777 = new ClassSeat(seatsperClass777);
        //loading available seats per class
        Map<ClassType,Integer> availableseatsperClass777 = new HashMap<>();
        availableseatsperClass777.put(ClassType.FIRSTCLASS,5);
        availableseatsperClass777.put(ClassType.BUSINESSCLASS,15);
        availableseatsperClass777.put(ClassType.ECONOMY,95);
        ClassSeat availableclassSeats777 = new ClassSeat(availableseatsperClass777);
        //Price for each class
        Map<ClassType,Float> pricePerClass777 = new HashMap<>();
        pricePerClass777.put(ClassType.FIRSTCLASS, (float) 20000.00);
        pricePerClass777.put(ClassType.BUSINESSCLASS, (float) 13000);
        pricePerClass777.put(ClassType.ECONOMY, (float) 6000);

        //search results
        expectedResult.add( new Flight(777,new Route(1,777,"HYD","BLR",classSeats777,
                LocalDate.now().plusDays(1)),pricePerClass777,availableclassSeats777));

        SearchCriteria searchCriteria = new SearchCriteria("HYD","BLR",2,null,
                ClassType.ECONOMY);
        flightService = new FlightService(new FlightRepository());
        List<Flight> filteredFlight = flightService.search(searchCriteria);

        //FlightRepository flightRepository = mock(FlightRepository.class);
        flightService = mock(FlightService.class);
        when(flightService.search(searchCriteria)).thenReturn(expectedResult);

        assertEquals(15600,( filteredFlight.get(0).getTicketPrice()),0);
    }

    @Test
    //Test for Business class fare
    public void returnFlightBCPricesgivensearchCriteria()  {

        List<Flight> expectedResult = new ArrayList<>() ;
        //loading seats per class
        Map<ClassType,Integer> seatsperClass777 = new HashMap<>();
        seatsperClass777.put(ClassType.FIRSTCLASS,8);
        seatsperClass777.put(ClassType.BUSINESSCLASS,35);
        seatsperClass777.put(ClassType.ECONOMY,195);
        ClassSeat classSeats777 = new ClassSeat(seatsperClass777);
        //loading available seats per class
        Map<ClassType,Integer> availableseatsperClass777 = new HashMap<>();
        availableseatsperClass777.put(ClassType.FIRSTCLASS,5);
        availableseatsperClass777.put(ClassType.BUSINESSCLASS,15);
        availableseatsperClass777.put(ClassType.ECONOMY,95);
        ClassSeat availableclassSeats777 = new ClassSeat(availableseatsperClass777);
        //Price for each class
        Map<ClassType,Float> pricePerClass777 = new HashMap<>();
        pricePerClass777.put(ClassType.FIRSTCLASS, (float) 20000.00);
        pricePerClass777.put(ClassType.BUSINESSCLASS, (float) 13000);
        pricePerClass777.put(ClassType.ECONOMY, (float) 6000);

        //search results
        expectedResult.add( new Flight(777,new Route(1,777,"HYD","BLR",classSeats777,
                LocalDate.now().plusDays(3)),pricePerClass777,availableclassSeats777));

        SearchCriteria searchCriteria = new SearchCriteria("HYD","BLR",2,LocalDate.now().plusDays(3),
                ClassType.BUSINESSCLASS);
        flightService = new FlightService(new FlightRepository());
        List<Flight> filteredFlight = flightService.search(searchCriteria);

        //FlightRepository flightRepository = mock(FlightRepository.class);
        flightService = mock(FlightService.class);
        when(flightService.search(searchCriteria)).thenReturn(expectedResult);

        assertEquals(36400,( filteredFlight.get(0).getTicketPrice()),0);
    }
    @Test
    //Test for Business class fare
    public void returnFlightFCPricesgivensearchCriteria()  {

        List<Flight> expectedResult = new ArrayList<>() ;
        //loading seats per class
        Map<ClassType,Integer> seatsperClass777 = new HashMap<>();
        seatsperClass777.put(ClassType.FIRSTCLASS,8);
        seatsperClass777.put(ClassType.BUSINESSCLASS,35);
        seatsperClass777.put(ClassType.ECONOMY,195);
        ClassSeat classSeats777 = new ClassSeat(seatsperClass777);
        //loading available seats per class
        Map<ClassType,Integer> availableseatsperClass777 = new HashMap<>();
        availableseatsperClass777.put(ClassType.FIRSTCLASS,5);
        availableseatsperClass777.put(ClassType.BUSINESSCLASS,15);
        availableseatsperClass777.put(ClassType.ECONOMY,95);
        ClassSeat availableclassSeats777 = new ClassSeat(availableseatsperClass777);
        //Price for each class
        Map<ClassType,Float> pricePerClass777 = new HashMap<>();
        pricePerClass777.put(ClassType.FIRSTCLASS, (float) 20000.00);
        pricePerClass777.put(ClassType.BUSINESSCLASS, (float) 13000);
        pricePerClass777.put(ClassType.ECONOMY, (float) 6000);

        //search results
        expectedResult.add( new Flight(777,new Route(1,777,"HYD","BLR",classSeats777,
                LocalDate.now().plusDays(3)),pricePerClass777,availableclassSeats777));

        SearchCriteria searchCriteria = new SearchCriteria("HYD","BLR",2,LocalDate.now().plusDays(3),
                ClassType.FIRSTCLASS);
        flightService = new FlightService(new FlightRepository());
        List<Flight> filteredFlight = flightService.search(searchCriteria);

        //FlightRepository flightRepository = mock(FlightRepository.class);
        flightService = mock(FlightService.class);
        when(flightService.search(searchCriteria)).thenReturn(expectedResult);

        assertEquals(68000,( filteredFlight.get(0).getTicketPrice()),0);
    }
}